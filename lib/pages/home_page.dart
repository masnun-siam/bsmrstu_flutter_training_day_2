import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_state_management_test/pages/details_page.dart';
import 'package:provider_state_management_test/providers/user_provider.dart';

import '../models/user_model.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController? nameController;
  TextEditingController? emailController;
  TextEditingController? ageController;

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController();
    emailController = TextEditingController();
    ageController = TextEditingController();
  }

  @override
  void dispose() {
    nameController?.dispose();
    emailController?.dispose();
    ageController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Users list'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
            context: context,
            isScrollControlled: true,
            builder: (context) {
              return Padding(
                padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: nameController,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Name',
                          label: Text('Name'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: emailController,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Email',
                          label: Text('Email'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: ageController,
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Age',
                          label: Text('Age'),
                        ),
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        if (nameController!.text.isEmpty) {
                          return;
                        }
                        if (emailController!.text.isEmpty) {
                          return;
                        }
                        if (ageController!.text.isEmpty) {
                          return;
                        }
                        final user = User(
                          name: nameController!.text,
                          email: emailController!.text,
                          age: int.tryParse(ageController!.text) ?? 0,
                        );

                        Provider.of<UserProvider>(context, listen: false)
                            .addUser(user);

                        nameController!.clear();
                        emailController!.clear();
                        ageController!.clear();

                        Navigator.pop(context);
                      },
                      child: const Text('Submit'),
                    ),
                  ],
                ),
              );
            },
          );
        },
        child: const Icon(Icons.add),
      ),
      body: Consumer<UserProvider>(
        builder: (context, userProvider, _) {
          return ListView.builder(
            itemCount: userProvider.users.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => DetailsPage(
                        user: userProvider.users[index],
                      ),
                    ),
                  );
                },
                onLongPress: () {
                  nameController!.text = userProvider.users[index].name;
                  emailController!.text = userProvider.users[index].email;
                  ageController!.text =
                      userProvider.users[index].age.toString();
                  showModalBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    builder: (context) {
                      return Padding(
                        padding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).viewInsets.bottom,
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: TextField(
                                controller: nameController,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: 'Name',
                                  label: Text('Name'),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: TextField(
                                controller: emailController,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: 'Email',
                                  label: Text('Email'),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: TextField(
                                controller: ageController,
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: 'Age',
                                  label: Text('Age'),
                                ),
                              ),
                            ),
                            ElevatedButton(
                              onPressed: () {
                                if (nameController!.text.isEmpty) {
                                  return;
                                }
                                if (emailController!.text.isEmpty) {
                                  return;
                                }
                                if (ageController!.text.isEmpty) {
                                  return;
                                }
                                final user = User(
                                  name: nameController!.text,
                                  email: emailController!.text,
                                  age: int.tryParse(ageController!.text) ?? 0,
                                );

                                Provider.of<UserProvider>(context,
                                        listen: false)
                                    .editUser(index, user);

                                nameController!.clear();
                                emailController!.clear();
                                ageController!.clear();

                                Navigator.pop(context);
                              },
                              child: const Text('Update'),
                            ),
                          ],
                        ),
                      );
                    },
                  );
                },
                child: Dismissible(
                  key: UniqueKey(),
                  onDismissed: (_) {
                    Provider.of<UserProvider>(context, listen: false)
                        .deleteUser(index);
                  },
                  child: ListTile(
                    title: Text(
                      userProvider.users[index].name,
                    ),
                    subtitle: Text(
                      userProvider.users[index].email,
                    ),
                    trailing: Text(
                      userProvider.users[index].age.toString(),
                    ),
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
