import 'package:flutter/material.dart';
import 'package:provider_state_management_test/models/user_model.dart';

class DetailsPage extends StatelessWidget {
  const DetailsPage({super.key, required this.user});

  final User user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.name),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(user.email),
          const SizedBox(
            height: 20,
            width: double.infinity,
          ),
          Text(user.age.toString()),
        ],
      ),
    );
  }
}
