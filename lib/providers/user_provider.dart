import 'package:flutter/material.dart';
import 'package:provider_state_management_test/models/user_model.dart';

class UserProvider extends ChangeNotifier {
  List<User> users = [];

  void addUser(User user) {
    users.add(user);
    notifyListeners();
  }

  void deleteUser(int index) {
    users.removeAt(index);
    notifyListeners();
  }

  void editUser(int index, User user) {
    users[index] = user;
    notifyListeners();
  }
}
